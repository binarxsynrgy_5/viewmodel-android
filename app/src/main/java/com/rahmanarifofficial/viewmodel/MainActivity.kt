package com.rahmanarifofficial.viewmodel

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.rahmanarifofficial.viewmodel.databinding.MainActivityBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    fun setupView() {
        binding.btnIncrement.setOnClickListener {
            onIncrement()
        }

        binding.btnDecrement.setOnClickListener {
            onDecrement()
        }

        val observerCounter = Observer<Int> {
            binding.tvCounter.text = it.toString()
        }

        viewModel.counter.observe(this, observerCounter)

    }

    fun onIncrement() {
        viewModel.increment()
    }

    fun onDecrement() {
        viewModel.decrement()
    }
}