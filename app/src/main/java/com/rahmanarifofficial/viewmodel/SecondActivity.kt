package com.rahmanarifofficial.viewmodel

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rahmanarifofficial.viewmodel.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    private var _binding: ActivitySecondBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPreferences: SharedPreferences

    private val viewModel: SecondViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initObject()
        loadMenu()
        setupView()
    }

    fun initObject() {
        sharedPreferences = SharedPreferences(this)
    }

    fun loadMenu() {
        viewModel.loadData()
    }

    fun setupView() {
        viewModel.data.observe(this) {
            val adapter = MenuAdapter(it)
            binding.recyclerViewMenu.adapter = adapter
            binding.recyclerViewMenu.layoutManager = LinearLayoutManager(this)
        }

        binding.btnAdd.setOnClickListener {
            sharedPreferences.isLogin = true
            viewModel.setLogin(sharedPreferences.isLogin)
        }

        viewModel.setLogin(sharedPreferences.isLogin)

        viewModel.isLogin.observe(this) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}