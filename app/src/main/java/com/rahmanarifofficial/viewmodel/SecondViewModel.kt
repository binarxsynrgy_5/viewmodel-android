package com.rahmanarifofficial.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SecondViewModel : ViewModel() {

    val sampleData = SampleData.menus
    private val _data: MutableLiveData<MutableList<String>> = MutableLiveData()
    val data: LiveData<MutableList<String>> get() = _data

    private val _isLogin: MutableLiveData<Boolean> = MutableLiveData()
    val isLogin: LiveData<Boolean> get() = _isLogin

    fun loadData() {
        _data.postValue(sampleData)
    }

    fun addData() {
        sampleData.add("Ayam Pop")
        _data.postValue(sampleData)
    }

    fun setLogin(value: Boolean){
        _isLogin.postValue(value)
    }
}