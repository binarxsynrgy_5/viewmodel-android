package com.rahmanarifofficial.viewmodel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rahmanarifofficial.viewmodel.databinding.ItemMenuBinding

class MenuAdapter(
    private val itemList: List<String>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DefaultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DefaultViewHolder -> {
                holder.bindItem(itemList[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}

class DefaultViewHolder(val itemBinding: ItemMenuBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {
    fun bindItem(
        item: String
    ) {
        itemBinding.textMenu.text = item
    }
}
