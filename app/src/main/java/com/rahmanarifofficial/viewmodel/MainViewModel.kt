package com.rahmanarifofficial.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    /*
    * Membuat logic untuk penambahan dan juga pengurangan
    * var -> mutable
    * val -> immutable
    * a++ -> a = a + 1
    * */
    val counter: MutableLiveData<Int> = MutableLiveData(0)

    fun increment() {
        counter.postValue(counter.value?.plus(1))
    }

    fun decrement() {
        counter.value?.let {
            if (it > 0) {
                counter.postValue(counter.value?.minus(1))
            }
        }
    }
}