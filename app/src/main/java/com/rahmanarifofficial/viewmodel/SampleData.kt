package com.rahmanarifofficial.viewmodel

object SampleData {
    val menus: MutableList<String>
        get() = mutableListOf(
            "Nasi Uduk",
            "Nasi Pecel",
            "Ayam Goreng",
            "Nasi Goreng",
            "Mie Goreng"
        )
}